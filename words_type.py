import arcade
import arcade.gui
import os

file_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(file_path)

WIDTH = 800
HEIGHT = 600



class InstructionView(arcade.Window):

    def __init__(self):
        super().__init__(800, 600, "yayaTaytaApp", resizable=True)

        # --- Required for all code that uses UI element,
        # a UIManager to handle the UI.
        self.manager = arcade.gui.UIManager()
        self.manager.enable()
        # Set background color
        arcade.set_background_color(arcade.color.DARK_BLUE_GRAY)

        # Create a vertical BoxGroup to align buttons
        self.v_box = arcade.gui.UIBoxLayout()
    
        ui_text_label = arcade.gui.UITextArea(text="                           Tipo de Palabras",
                                              width=780,
                                              height=40,
                                              font_size=24,
                                              font_name="Kenney Future")
        self.v_box.add(ui_text_label.with_space_around(bottom=0))


        # Crear Botones
        ui_flatbutton = arcade.gui.UIFlatButton(text="Palabras en Kchwa", width=200)
        self.v_box.add(ui_flatbutton.with_space_around(bottom=20))
        #on_click_flatbutton.clicked.connect(lambda: self.InstructionView())
        #declarar instancia de la clase 
        

        # Cree un widget para contener el widget v_box, que centrará los botones
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x",
                anchor_y="center_y",
                child=self.v_box)
        )

    def on_draw(self):
        self.clear()
        self.manager.draw()




