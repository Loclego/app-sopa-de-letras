import arcade
import arcade.gui
from arcade.gui.events import UIOnClickEvent
from words_type import InstructionView
import os

file_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(file_path)

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

MENU_SCREEN = 0

class MyWindow(arcade.Window):

    def __init__(self):
        super().__init__(800, 600, "yayaTaytaApp", resizable=True)
        self.background = None
        # --- Required for all code that uses UI element,
        # a UIManager to handle the UI.
        self.manager = arcade.gui.UIManager()
        self.manager.enable()
        # Set background color
        arcade.set_background_color(arcade.color.BROWN_NOSE)
        # Create a vertical BoxGroup to align buttons
        self.v_box = arcade.gui.UIBoxLayout()

        ui_text_label = arcade.gui.UITextArea(text="                 Nombre de Usuario",
                                              width=700,
                                              height=40,
                                              font_size=24,
                                              font_name="Kenney Future")
        self.v_box.add(ui_text_label.with_space_around(bottom=0))
        
        
        self.ui_text_label_name = arcade.gui.UIInputText(text="",
                                              width=200,
                                              height=40,
                                              font_size=24,
                                              font_name="Kenney Future")
        self.v_box.add(self.ui_text_label_name.with_space_around(bottom=0))

        ui_text_label_age = arcade.gui.UITextArea(text="    Edad",
                                              width=200,
                                              height=40,
                                              font_size=24,
                                              font_name="Kenney Future")
        self.v_box.add(ui_text_label_age.with_space_around(bottom=0))
        self.ui_text_label_age = arcade.gui.UIInputText(text="",
                                              width=200,
                                              height=40,
                                              font_size=24,
                                              font_name="Kenney Future")
        self.v_box.add(self.ui_text_label_age.with_space_around(bottom=0))
        ui_text_label = arcade.gui.UITextArea(text="Dificultad",
                                              width=250,
                                              height=40,
                                              font_size=24,
                                              font_name="Kenney Future")
        self.v_box.add(ui_text_label.with_space_around(bottom=0))
        

        # Crear Botones
        ui_flatbutton_hard = arcade.gui.UIFlatButton(text="Dificil", width=100)
        self.v_box.add(ui_flatbutton_hard.with_space_around(bottom=20))
        ui_flatbutton_midle = arcade.gui.UIFlatButton(text="Medio", width=100)
        self.v_box.add(ui_flatbutton_midle.with_space_around(bottom=20))
        ui_flatbutton_easy = arcade.gui.UIFlatButton(text="Facil", width=100)
        self.v_box.add(ui_flatbutton_easy.with_space_around(bottom=20))
        
        ui_flatbutton_start = arcade.gui.UIFlatButton(text="Iniciar",width=200)
        self.v_box.add(ui_flatbutton_start.with_space_around(bottom=20))
       
        # Gestionar Click del Boton Iniciar        
        ui_flatbutton_start.on_click = self.on_click_flatbutton

        
        # Manejar eventos del click Salir
        quit_button = QuitButton(text="Salir", width=200,font_name="Kenney Future")
        self.v_box.add(quit_button)
      
        
        # Cree un widget para contener el widget v_box, que centrará los botones
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x",
                anchor_y="center_y",
                child=self.v_box)
        )
    
    def on_draw(self):
        self.clear()
        self.manager.draw()

    def on_click_flatbutton(self, event):

        self.age = self.ui_text_label_age.text
        self.name = self.ui_text_label_name.text

        if self.age == '' or self.name == '':
            pass
        else:
            self.close()
            misegundaapp=InstructionView()        
            arcade.run()
            #Crear metodo View
               
        

class QuitButton(arcade.gui.UIFlatButton):
    def on_click(self, event: arcade.gui.UIOnClickEvent):
        arcade.exit()

